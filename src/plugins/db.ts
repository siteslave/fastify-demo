var fastifyPlugin = require('fastify-plugin');
var knex = require('knex');

async function fastifyKnexJS(fastify: any, opts: any, next: any) {
  try {
    const handler = await knex(opts.connection);
    fastify.decorate(opts.connectionName, handler);
    next();
  } catch (err) {
    next(err);
  }
}

module.exports = fastifyPlugin(fastifyKnexJS, '>=0.30.0');
