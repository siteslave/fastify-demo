import * as HttpStatus from 'http-status-codes';
var fastifyPlugin = require('fastify-plugin');

async function fastifyJwt(fastify: any, opts: any, next: any) {
  fastify.register(require("fastify-jwt"), {
    secret: opts.secret
  })

  try {
    fastify.decorate("authenticate", async function (req: any, reply: any) {

      let token = null;

      if (req.headers.authorization && req.headers.authorization.split(' ')[0].toLowerCase() === 'bearer') {
        token = req.headers.authorization.split(' ')[1];
      } else if (req.query && req.query.token) {
        token = req.query.token;
      } else if (req.body && req.body.token) {
        token = req.body.token;
      } else {
        token = null;
      }

      if (token) {
        try {
          await req.jwtVerify(token);
          next();
        } catch (err) {
          reply.code(HttpStatus.UNAUTHORIZED).send({ code: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
        }
      } else {
        reply.code(HttpStatus.UNAUTHORIZED).send({ code: HttpStatus.UNAUTHORIZED, message: HttpStatus.getStatusText(HttpStatus.UNAUTHORIZED) });
      }

    });

  } catch (err) {
    next(err)
  }
}

module.exports = fastifyPlugin(fastifyJwt, '>=0.30.0');
