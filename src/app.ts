/// <reference path="../typings.d.ts" />

import path = require('path');

import * as http from 'http'
import * as fastify from 'fastify'
import * as moment from 'moment';

require('dotenv').config({ path: path.join(__dirname, '../config') });

const app: fastify.FastifyInstance = fastify.fastify({})

app.register(require('fastify-formbody'));
app.register(require('fastify-cors'), {});
app.register(require('fastify-no-icon'));

app.register(require('fastify-rate-limit'), {
  max: +process.env.MAX_CONNECTION_PER_MINUTE || 500,
  timeWindow: '1 minute',
  whitelist: ['127.0.0.1', '192.168.1.1']
  // global: false
});

app.register(require('./plugins/jwt'), {
  secret: process.env.SECRET_KEY
});

app.register(require('./plugins/db'), {
  connection: {
    client: process.env.DB_CLIENT || 'mysql',
    connection: {
      host: process.env.DB_HOST || 'localhost',
      user: process.env.DB_USER || 'root',
      port: +process.env.DB_PORT || 3306,
      password: process.env.DB_PASSWORD,
      database: process.env.DB_NAME || 'test',
    },
    pool: {
      min: 0,
      max: 100
    },
    debug: +process.env.DEBUG === 1 ? true : false,
  },
  connectionName: 'db'
});

app.register(require('./routes/index'), { prefix: '/', logger: true });
app.register(require('./routes/cannabis'), { prefix: '/cannabis', logger: true });
app.register(require('./routes/api'), { prefix: '/api', logger: true });
app.register(require('./routes/test'), { prefix: '/test', logger: true });
app.register(require('./routes/users'), { prefix: '/users', logger: true });

app.get('/', {
  config: {
    rateLimit: {
      max: 3,
      timeWindow: '1 minute'
    }
  }
}, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
  const version = process.env.VERSION || '0.0.1';
  const build = process.env.BUILD_VERSION || moment().format('YYYYMMDDHHmm');

  reply.code(200).send({
    message: 'Welcome API services!',
    version: version,
    build: build
  })
});

const port = +process.env.PORT || 3000;
const host = process.env.HTTP_BINDING || '0.0.0.0';

const start = async () => {
  try {
    await app.listen(port, host);
    const info: any = app.server.address();
    console.log(`server listening on http://${info.address}:${info.port}`);
    app.log.info(`server listening on http://${info.address}:${info.port}`);
  } catch (err) {
    app.log.error(err);
    process.exit(1);
  }
}

start();
