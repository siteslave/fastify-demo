import * as knex from 'knex';

export class HISModel {
  testing(db: knex) {
    return db('ccd_person')
      .select('hospcode')
      .limit(1);
  }

  searchPerson(db: knex, cid: any) {
    return db('ccd_person').where('cid', cid).limit(1);
  }

  searchVisit(db: knex, hn: any, startDate: any, endDate: any) {
    return db('ccd_opd_visit')
      .where('hn', hn)
      .whereBetween('vstdate', [startDate, endDate])
      .orderByRaw('vstdate DESC, vsttime DESC')
      .limit(100);
  }

  patientInfo(db: knex, hn: any) {
    return db('ccd_person')
      .where('hn', hn)
      .limit(1);
  }

  getVisitLab(db: knex, hn: any, vn: any) {
    return db('ccd_lab_result')
      .where('hn', hn)
      .where('vn', vn);
  }

  getVisitDrug(db: knex, hn: any, vn: any) {
    return db('ccd_dispense_items')
      .where('hn', hn)
      .where('vn', vn);
  }

  getVisitAppointment(db: knex, hn: any, vn: any) {
    return db('ccd_appointment')
      .where('hn', hn)
      .where('vn', vn);
  }

  getVisitDiagText(db: knex, hn: any, vn: any) {
    return db('ccd_opd_visit_diag_text')
      .where('hn', hn)
      .where('vn', vn);
  }

  getVisitDiagnosis(db: knex, hn: any, vn: any) {
    return db('ccd_opd_visit_diag')
      .where('hn', hn)
      .where('vn', vn);
  }

}