import * as knex from 'knex';
import { IUserColumns } from '../interfaces/IUser';

export class UserModel {
  getUser(db: knex) {
    return db('users').select('user_id', 'username', 'first_name', 'last_name');
  }

  info(db: knex, userId: any) {
    return db('users').where('user_id', userId);
  }

  update(db: knex, userId: any, user: IUserColumns) {
    return db('users')
      .where('user_id', userId)
      .update(user);
  }

  login(db: knex, username: any, password: any) {
    return db('users')
      .select('user_id', 'username', 'first_name', 'last_name')
      .where('username', username)
      .where('password', password);
  }

}