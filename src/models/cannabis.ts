import * as knex from 'knex';
import axios, { AxiosInstance, AxiosResponse } from 'axios';

export class Cannabis {
  http: AxiosInstance;

  constructor() {
    this.http = axios.create();
  }

  getSpclty(db: knex) {
    return db('spclty')
      .select('name', 'spclty')
      .orderBy('name', 'asc');
    // select name, spclty from spclty order by name asc
  }

  register(db: knex, user: object) {
    return db('test_users')
      .insert(user, 'user_id')
  }

  update(db: knex, userId: any, user: object) {
    return db('test_users')
      .where('user_id', userId)
      .update(user)
  }

  delete(db: knex, userId: any) {
    return db('test_users')
      .where('user_id', userId)
      .del();
  }

  login(db: knex, username: any, password: any) {
    return db('test_users')
      .select('user_id', 'first_name', 'last_name')
      // .where('username', username)
      // .where('password', password)
      .where({
        username,
        password
      });
  }

  async getCoWard(): Promise<AxiosResponse<any>> {
    return this.http.get(`http://covid19data.moph.go.th/api/v3/resourceFilterDataAll`, {
      headers: {
        'Authorization': 'Basic U2ZGRFd4OlFaMnZLVndnM3V0QXRmSHQ='
      },
    });
  }

}