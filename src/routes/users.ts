/// <reference path="../../typings.d.ts" />

import * as knex from 'knex';
import * as fastify from 'fastify';
import * as crypto from 'crypto';

import { UserModel } from '../models/user';
import { IUserColumns } from '../interfaces/IUser';

const userModel = new UserModel();

const router = (fastify: any, { }, next: any) => {

  var db: knex = fastify.db;

  fastify.put('/info', {
    preHandler: [fastify.authenticate],
    schema: {
      body: {
        type: 'object',
        properties: {
          firstName: { type: 'string' },
          lastName: { type: 'string' },
          email: {
            type: 'string',
            pattern: '^[a-z0-9\._%+!$&*=^|~#%{}/\-]+@([a-z0-9\-]+\.){1,}([a-z]{2,22})$',
          },
          phoneNumber: {
            type: 'string',
            pattern: '^[0-9]{10}$'
          },
          birthdate: {
            type: 'string',
            pattern: "^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$"
          },
          sex: { type: 'number', enum: [1, 2] }
        },
        required: ['firstName', 'lastName', 'email', 'sex', 'phoneNumber', 'birthdate']

      },
      response: {
        200: {
          type: 'object',
          properties: {
            ok: { type: 'boolean' },
            error: { type: 'string' }
          }
        }
      }
    }
  }, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    const data: any = req.body;
    const firstName = data.firstName;
    const lastName = data.lastName;
    const email = data.email;
    const phoneNumber = data.phoneNumber;
    const birthdate = data.birthdate;
    const sex = data.sex;
    const userId = req.user.userId;

    try {
      const user: IUserColumns = {};
      user.first_name = firstName;
      user.last_name = lastName;
      user.email = email;
      user.phone_number = phoneNumber;
      user.birthdate = birthdate;
      user.sex = sex;

      await userModel.update(db, userId, user);
      reply.send({ ok: true });
    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]', code: 500 });
    }
  })

  fastify.get('/info', {
    preHandler: [fastify.authenticate]
  }, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    console.log(req.user);
    try {
      const userId = req.user.userId;
      const rs = await userModel.info(db, userId);
      reply.code(200).send({ ok: true, info: rs[0] });
    } catch (error) {
      req.log.error(error);
      reply.code(500).send({ ok: false, message: error.message, code: 500 });
    }
  });

  next();

}

module.exports = router;