/// <reference path="../../typings.d.ts" />

import * as knex from 'knex';
import * as fastify from 'fastify';
import * as crypto from 'crypto';

import { UserModel } from '../models/user';

const userModel = new UserModel();

const router = (fastify: any, { }, next: any) => {

  var db: knex = fastify.db;

  fastify.post('/login', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    const data: any = req.body;
    const username = data.username;
    const password = data.password;

    if (username && password) {
      try {
        const encPassword = crypto.createHash('md5').update(password).digest('hex');
        const rs: any = await userModel.login(db, username, encPassword);
        if (rs.length > 0) {
          const user: any = rs[0];
          const token = fastify.jwt.sign({
            userId: user.user_id,
            firstName: user.first_name,
            lastName: user.last_name,
          }, { expiresIn: '1d' });
          reply.send({ ok: true, token });
        } else {
          reply.send({ ok: false, error: 'ชื่อผู้ใช้งานหรือรหัสผ่าน ไม่ถูกต้อง', code: 500 });
        }
      } catch (error) {
        console.log(error);
        reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
      }
    } else {
      reply.send({ ok: false, message: 'กรุณาระบุข้อมูลให้ครบ', code: 402 });
    }
  })

  fastify.get('/token', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    const token = fastify.jwt.sign({ foo: 'bar' }, { expiresIn: '1d' });
    reply.send({ token: token });
  })

  fastify.get('/private', {
    preHandler: [fastify.authenticate]
  }, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    console.log(req.user);
    try {
      var rs = await userModel.getUser(db);
      reply.code(200).send({ ok: true, rows: rs });
    } catch (error) {
      req.log.error(error);
      reply.code(500).send({ ok: false, message: error.message });
    }
  });

  next();

}

module.exports = router;