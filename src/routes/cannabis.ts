/// <reference path="../../typings.d.ts" />

import * as knex from 'knex';
import * as fastify from 'fastify';
import * as crypto from 'crypto';

import { Cannabis } from '../models/cannabis';
import { AxiosResponse } from 'axios';

const router = (fastify: any, { }, next: any) => {

  var db: knex = fastify.db;
  const cannabisModel = new Cannabis();

  fastify.post('/login', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;
      const username = body.username;
      const password = body.password;
      const encPassword = crypto.createHash('md5').update(password).digest('hex');
      const rs: any = await cannabisModel.login(db, username, encPassword);

      if (rs.length > 0) {
        const payload: any = {
          userId: rs[0].user_id,
          firstName: rs[0].first_name,
          lastName: rs[0].last_name
        };
        const token = fastify.jwt.sign(payload, { expiresIn: '1h' });
        reply.send({ token: token });
      } else {
        reply.send({ ok: false, error: 'ชื่อผู้ใช้งานหรือรหัสผ่าน ไม่ถูกต้อง' });
      }

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
    }
  })

  fastify.get('/spclty', {
    preHandler: [fastify.authenticate]
  }, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const rs = await cannabisModel.getSpclty(db);
      reply.send(rs);
    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
    }
  })

  fastify.get('/coward', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const rs: AxiosResponse = await cannabisModel.getCoWard();
      reply.send(rs.data);
    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
    }
  })

  fastify.delete('/users/:userId', {
    preHandler: [fastify.authenticate]
  }, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const params: any = req.params;
      const userId: any = params.userId;

      await cannabisModel.delete(db, userId);
      reply.send({ ok: true });

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
    }
  })

  fastify.post('/register', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const params: any = req.body;
      const username = params.username;
      const password = params.password;
      const firstName = params.firstName;
      const lastName = params.lastName;

      const encPassword = crypto.createHash('md5').update(password).digest('hex');

      const user: any = {};
      user.username = username;
      user.first_name = firstName;
      user.last_name = lastName;
      user.password = encPassword;

      const rs: any = await cannabisModel.register(db, user);
      // rs[0] = user_id
      reply.send(rs[0]);

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
    }
  })

  fastify.put('/update/:userId', {
    preHandler: [fastify.authenticate]
  }, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {


      const body: any = req.body;
      const params: any = req.params;

      const userId = params.userId;
      const password = body.password;
      const firstName = body.firstName;
      const lastName = body.lastName;

      const user: any = {};
      user.first_name = firstName;
      user.last_name = lastName;

      if (password) {
        var encPassword = crypto.createHash('md5').update(password).digest('hex');
        user.password = encPassword;
      }

      await cannabisModel.update(db, userId, user);
      // rs[0] = user_id
      reply.send({ ok: true });

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
    }
  })

  next();

}

module.exports = router;