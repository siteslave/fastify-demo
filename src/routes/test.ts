/// <reference path="../../typings.d.ts" />

import * as knex from 'knex';
import * as fastify from 'fastify';
import * as crypto from 'crypto';

import { HISModel } from '../models/his';

const hisModel = new HISModel();

const router = (fastify: any, { }, next: any) => {

  var db: knex = fastify.db;

  fastify.get('/', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const rs: any = await hisModel.testing(db);
      reply.send(rs[0]);

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: 'เกิดข้อผิดพลาด [server]' });
    }
  })

  fastify.get('/token', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    const token = fastify.jwt.sign({ foo: 'bar' }, { expiresIn: '1d' });
    reply.send({ token: token });
  })

  // fastify.get('/private', {
  //   preHandler: [fastify.authenticate]
  // }, async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
  //   console.log(req.user);
  //   try {
  //     var rs = await userModel.getUser(db);
  //     reply.code(200).send({ ok: true, rows: rs });
  //   } catch (error) {
  //     req.log.error(error);
  //     reply.code(500).send({ ok: false, message: error.message });
  //   }
  // });

  next();

}

module.exports = router;