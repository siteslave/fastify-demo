/// <reference path="../../typings.d.ts" />

import * as knex from 'knex';
import * as fastify from 'fastify';
import * as moment from 'moment';

import { HISModel } from '../models/his';

const hisModel = new HISModel();

const router = (fastify: any, { }, next: any) => {

  const db: knex = fastify.db;

  fastify.post('/search/person/cid', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;

      const rs: any = await hisModel.searchPerson(db, body.cid);
      reply.send(rs);

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: error.message });
    }
  })

  fastify.post('/search/visit', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;
      const hn = body.hn;
      const startDate = body.startDate;
      const endDate = body.endDate;

      const rs: any = await hisModel.searchVisit(db, hn, startDate, endDate);
      const visit = rs.map((v: any) => {
        v.vstdate = moment(v.vstdate).format('YYYY-MM-DD');
        return v;
      });

      reply.send(visit);

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: error.message });
    }
  })


  fastify.post('/patient/info', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;
      const hn = body.hn;

      const rs: any = await hisModel.patientInfo(db, hn);
      reply.send(rs);

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: error.message });
    }
  })

  fastify.post('/visit/lab', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;
      const hn = body.hn;
      const vn = body.vn;

      const rs: any = await hisModel.getVisitLab(db, hn, vn);
      const visit = rs.map((v: any) => {
        v.order_date = moment(v.order_date).format('YYYY-MM-DD');
        return v;
      });

      reply.send(visit);

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: error.message });
    }
  })

  fastify.post('/visit/drug', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;
      const hn = body.hn;
      const vn = body.vn;

      const rs: any = await hisModel.getVisitDrug(db, hn, vn);
      reply.send(rs);

    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: error.message });
    }
  })

  fastify.post('/visit/diag-text', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;
      const hn = body.hn;
      const vn = body.vn;

      const rs: any = await hisModel.getVisitDiagText(db, hn, vn);
      reply.send(rs);
    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: error.message });
    }
  })

  fastify.post('/visit/diagnosis', async (req: fastify.FastifyRequest, reply: fastify.FastifyReply) => {
    try {
      const body: any = req.body;
      const hn = body.hn;
      const vn = body.vn;

      const rs: any = await hisModel.getVisitDiagnosis(db, hn, vn);
      reply.send(rs);
    } catch (error) {
      console.log(error);
      reply.send({ ok: false, error: error.message });
    }
  })

  next();

}

module.exports = router;