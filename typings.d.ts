import { FastifyInstance, FastifyRequest, FastifyReply } from 'fastify';
import { Server, IncomingMessage, ServerResponse } from 'http';
import * as knex from 'knex'

declare module 'fastify' {
  interface FastifyRequest {
    user: any;
  }
  // interface FastifyReply<HttpResponse> { }
  // interface Request extends FastifyRequest<IncomingMessage> { }
  // interface Reply extends FastifyReply<ServerResponse> { }
  interface FastifyInstance {
    knex: knex;
  }
}

